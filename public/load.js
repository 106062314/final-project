var loadState = {
    preload: function(){
        var progressBar = game.add.sprite(400, 350, 'progressBar');
        game.load.setPreloadSprite(progressBar);

        // Load images.
        game.load.image('menu', 'assets/menu.jpg');
        game.load.image('title', 'assets/title.png');
        game.load.image('background', 'assets/background.png');
        game.load.image('background2', 'assets/background2.png');
        game.load.image('bullet', 'assets/bullet.png');
        game.load.image('bullet2', 'assets/bullet2.png');
        game.load.image('enemyBullet', 'assets/enemy-bullet.png');
        game.load.image('life', 'assets/life.png');
        game.load.image('bomb', 'assets/bomb.png');
        game.load.image('power', 'assets/power.png');
        game.load.image('point', 'assets/point.png');
        game.load.image('hitbox', 'assets/hitbox.png');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('healthbar', 'assets/healthbar.png');
        // Load spritesheets.
        game.load.spritesheet('player', 'assets/player.png', 32, 48);
        game.load.spritesheet('enemy-xs', 'assets/enemy-s.png', 32, 32);
        game.load.spritesheet('enemy-s', 'assets/enemy-s.png', 32, 32);
        game.load.spritesheet('enemy-m', 'assets/enemy-m.png', 64, 64);
        game.load.spritesheet('enemy-l', 'assets/enemy-l.png', 32, 36);
        game.load.spritesheet('explode', 'assets/explode.png', 128, 128);
        game.load.spritesheet('sparkle', 'assets/sparkle.png', 64, 64);
        game.load.spritesheet('boss', 'assets/boss.png', 48, 64);
        game.load.spritesheet('laser', 'assets/laser2.png', 32, 512);
        // Load audios.
        game.load.audio('menu', 'assets/audio/menu.mp3');
        game.load.audio('bgm', 'assets/audio/bgm.mp3');
        game.load.audio('explosionSE', 'assets/audio/explosion.mp3');
        game.load.audio('itemSE', 'assets/audio/item.wav');
        game.load.audio('pldeadSE', 'assets/audio/pldead.wav');
        game.load.audio('plshotSE', 'assets/audio/plshot.wav');
        game.load.audio('powerupSE', 'assets/audio/powerup.wav');
        game.load.audio('skillSE', 'assets/audio/skill.wav');
        game.load.audio('damageSE', 'assets/audio/damage.wav');
        game.load.audio('enemyshotSE', 'assets/audio/tan00.wav');
        game.load.audio('bossdeadSE', 'assets/audio/enep01.wav');
    },
    create: function(){
        game.state.start('menu');
    }
}