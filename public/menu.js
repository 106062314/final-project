var scores = [];
var names = [];
var length = 0;
var menuState = {
    create: function(){
        game.stage.backgroundColor = '#000000';
        game.add.image(0, 0, 'menu');

        var nameLabel = game.add.image(-100, 20, 'title');
        game.add.tween(nameLabel).to({x: 10}, 1200).easing(Phaser.Easing.Power1).start();

        var startLabel = game.add.text(game.width/2, 400, 'Press Enter to start', {font: '22px Arial', fill: '#ffffff'})
        startLabel.anchor.setTo(0.5, 0.5);
        game.add.tween(startLabel).to({alpha: 0}, 1000).yoyo(true).loop().start();

        var leaderboardKey = game.input.keyboard.addKey(Phaser.Keyboard.L);
        leaderboardKey.onDown.add(this.showLeaderboard, this);

        this.initLeaderboard();
        setTimeout(()=>{
            var startKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
            startKey.onDown.add(this.start, this);
        }, 2000);

        this.music = game.add.audio('menu');
        this.music.play();
    },
    start: function(){
        this.music.stop();
        game.state.start('play');
    },
    initLeaderboard: function(){
        scores = [];
        names = [];
        length = 0;
        var leaderboardRef = firebase.database().ref('leaderboard');
        leaderboardRef.once('value')
        .then(function(snapshot) {
            var obj = snapshot.val();
            for(var key in obj) {
                scores[length] = obj[key].score;
                names[length] = (obj[key].name == undefined) ? "Noname" : obj[key].name;
                length++;
            }
            // Sort with score.
            for(var i=0;i<length;i++){
                for(var j=i;j<length;j++){
                    if(scores[i] < scores[j]){
                        var tmp = scores[i];
                        scores[i] = scores[j];
                        scores[j] = tmp;
                        tmp = names[i];
                        names[i] = names[j];
                        names[j] = tmp;
                    }
                }
            }
            game.global.highscoreDB = scores[0];
            if(length > 10) length = 10;
        })
        .catch(e => console.log(e.message));

        // game.global.highscoreDB = bestScore;
    },
    showLeaderboard: function(){
        var content = "Highscore\n\n";
        for(var i=0;i<length;i++){
            content += i+1 + ". " + names[i] + "   " + scores[i] + "\n";
        }
        alert(content);
    }
}