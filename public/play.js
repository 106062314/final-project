const gameWidth = 640;
const gameHeight = 480;
const fieldWidth = 440;
var playState = {
    preload: function(){
    },
    create: function(){
        // Physics engine.
        // game.physics.startSystem(Phaser.Physics.ARCADE);
        // Background.
        game.stage.backgroundColor = '#3498db';
        this.background = game.add.tileSprite(0, 0, fieldWidth, gameHeight, 'background');
        game.world.setBounds(0, -100, fieldWidth+100, gameHeight+100);
        // UI.
        this.UI = game.add.image(fieldWidth, 0, 'background2');
        // Mask.
        this.mask = game.add.graphics();
        this.mask.beginFill(0xffffff);
        this.mask.drawRect(0, 0, fieldWidth, gameHeight);
        // Player.
        this.player = game.add.sprite(fieldWidth/2, gameHeight/2 + 200, 'player');
        game.physics.arcade.enable(this.player);
        this.player.anchor.setTo(0.5);
        this.player.animations.add('default', [ 0, 1, 2, 3, 4, 5, 6, 7 ], 20, true);
        this.player.body.collideWorldBounds = true;
        // Player's hitbox.
        this.hitbox = game.add.sprite(fieldWidth/2, gameHeight/2 + 200, 'hitbox');
        game.physics.arcade.enable(this.hitbox);
        this.hitbox.anchor.setTo(0.5);
        this.hitbox.scale.setTo(0.8);
        // Enemies.
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        this.enemies.mask = this.mask;
        // Bosses.
        this.bosses = game.add.group();
        this.bosses.enableBody = true;
        this.bosses.mask = this.mask;
        this.bossHealthBar = game.add.sprite(20, 20, 'healthbar');
        this.bossHealthBar.scale.x = 0;
        // Items.
        this.items = game.add.group();
        this.items.enableBody = true;
        this.items.mask = this.mask;
        // Your bullets.
        this.playerBullets = game.add.group();
        this.playerBullets.enableBody = true;
        this.playerBullets.createMultiple(80, 'bullet');
        this.playerBullets.setAll('anchor.x', 0.5);
        this.playerBullets.setAll('anchor.y', 0.5);
        this.playerBullets.setAll('outOfBoundsKill', true);
        this.playerBullets.setAll('checkWorldBounds', true);
        this.nextShotAt = 0;
        this.shotDelay = 100;
        this.relifeTime = 0;
        this.unbeatableTime = game.time.now + 5000;
        this.skillTime = 0;
        this.playerBullets.mask = this.mask;
        this.bulletAngle = 0;
        // Aiming bullets.
        this.aimingBullets = game.add.group();
        this.aimingBullets.enableBody = true;
        this.aimingBullets.createMultiple(30, 'bullet2');
        this.aimingBullets.setAll('anchor.x', 0.5);
        this.aimingBullets.setAll('anchor.y', 0.5);
        this.aimingBullets.setAll('outOfBoundsKill', true);
        this.aimingBullets.setAll('checkWorldBounds', true);
        this.aimingBullets.mask = this.mask;
        this.aimingBulletsCount = 0;
        // Laser beam.
        this.laser = game.add.sprite(fieldWidth/2, gameHeight/2 + 200, 'laser');
        game.physics.arcade.enable(this.laser);
        this.laser.anchor.setTo(0.5, 1);
        this.laser.alpha = 0.7;
        this.laser.animations.add('beam', [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ], 20, true);
        this.laser.animations.play('beam');
        this.laser.kill();
        // Enemies' bullets.
        this.enemyBullets = game.add.group();
        this.enemyBullets.enableBody = true;
        this.enemyBullets.createMultiple(100, 'enemyBullet');
        this.enemyBullets.setAll('anchor.x', 0.5);
        this.enemyBullets.setAll('anchor.y', 0.5);
        this.enemyBullets.setAll('outOfBoundsKill', true);
        this.enemyBullets.setAll('checkWorldBounds', true);
        this.enemyBullets.mask = this.mask;
        // Player's life.
        this.lives = game.add.group();
        // Player's ultmate skill.
        this.bombs = game.add.group();
        // Controlling keys.
        this.cursor = game.input.keyboard.createCursorKeys();

        // Timer.
        this.timer = 0;
        game.time.events.loop(Phaser.Timer.HALF, this.updateTimer, this);

        // Particle system.
        // game.renderer.renderSession.roundPixels = true;
        this.emitters = game.add.group();
        this.emitters.createMultiple(50);

        // Background music.
        this.bgm = game.add.audio('bgm', 1, true);
        this.bgm.play();
        // Sound effect.
        this.explosionSE = game.add.audio('explosionSE');
        this.itemSE = game.add.audio('itemSE');
        this.pldeadSE = game.add.audio('pldeadSE');
        this.plshotSE = game.add.audio('plshotSE');
        this.powerupSE = game.add.audio('powerupSE');
        this.skillSE = game.add.audio('skillSE');
        this.damageSE = game.add.audio('damageSE');
        this.enemyshotSE = game.add.audio('enemyshotSE');
        this.bossdeadSE = game.add.audio('bossdeadSE');
        game.sound.setDecodedCallback([ this.explosionSE, this.itemSE, this.pldeadSE, this.plshotSE, this.powerupSE, this.skillSE, this.damageSE, this.enemyshotSE, this.bossdeadSE ], this.update, this);

        this.gameover = false;
        this.level = 1;

        game.input.keyboard.addCallbacks(this, this.resume, null);

        // Info texts.
        this.highscore = game.global.highscoreDB;
        game.add.text(fieldWidth + 20, 30, 'Highscore', {font: '18px Arial', fill: '#ffffff'});
        this.highscoreLabel = game.add.text(fieldWidth + 120, 30, this.highscore, {font: '18px Arial', fill: '#ffffff'});
        
        this.score = 0;
        game.add.text(fieldWidth + 20, 60, 'Score', {font: '18px Arial', fill: '#ffffff'});
        this.scoreLabel = game.add.text(fieldWidth + 120, 60, this.score, {font: '18px Arial', fill: '#ffffff'});

        game.add.text(fieldWidth + 20, 120, 'Player', {font: '18px Arial', fill: '#ffffff'});
        for (var i = 1; i >= 0; i--) {
            this.life = this.lives.create(fieldWidth + 80 + (20 * i), 120, 'life');
            // this.life.anchor.setTo(0.5, 0.5);
            this.life.scale.setTo(0.7, 0.7);
        }

        game.add.text(fieldWidth + 20, 150, 'Bomb', {font: '18px Arial', fill: '#ffffff'});
        for (var i = 2; i >= 0; i--) {
            this.bomb = this.bombs.create(fieldWidth + 80 + (20 * i), 150, 'bomb');
            // this.bomb.anchor.setTo(0.5, 0.5);
            this.bomb.scale.setTo(0.7, 0.7);
        }

        this.playerPower = 0;
        game.add.text(fieldWidth + 20, 210, 'Power', {font: '18px Arial', fill: '#ffffff'});
        this.powerLabel = game.add.text(fieldWidth + 120, 210, this.playerPower, {font: '18px Arial', fill: '#ffffff'});
       
        this.playerPoint = 0;
        game.add.text(fieldWidth + 20, 240, 'Point', {font: '18px Arial', fill: '#ffffff'});
        this.pointLabel = game.add.text(fieldWidth + 120, 240, this.playerPower, {font: '18px Arial', fill: '#ffffff'});

        this.bgmVolume = 100;
        game.add.text(fieldWidth + 20, 300, 'Music', {font: '18px Arial', fill: '#ffffff'});
        this.bgmVolumeLabel = game.add.text(fieldWidth + 120, 300, this.bgmVolume, {font: '18px Arial', fill: '#ffffff'});
        this.soundVolume = 100;
        game.add.text(fieldWidth + 20, 330, 'Sound', {font: '18px Arial', fill: '#ffffff'});
        this.volumeLabel = game.add.text(fieldWidth + 120, 330, this.soundVolume, {font: '18px Arial', fill: '#ffffff'});

        this.stateLabel = game.add.text(fieldWidth/2, gameHeight/2, '', {font: '30px Arial', fill: '#ffffff'})
        this.stateLabel.anchor.setTo(0.5);

        // game.add.text(fieldWidth + 20, 460, '106062314  蔡政諺', {font: '18px 標楷體', fill: '#ffffff'});
    },
    update: function(){
        // Scroll the background.
        this.background.tilePosition.y += 1;

        // Reset player's velocity.
        this.player.body.velocity.setTo(0, 0);

        // Controlling player.
        if(!this.cursor.left.isDown && !this.cursor.right.isDown && !this.cursor.up.isDown && !this.cursor.down.isDown){
            this.player.animations.play('default');
        } else {
            if(!game.input.keyboard.isDown(Phaser.Keyboard.SHIFT)){
                if(this.cursor.left.isDown){
                    this.player.body.velocity.x += -200;
                }
                if(this.cursor.right.isDown && this.player.x < fieldWidth - 16){
                    this.player.body.velocity.x += 200;
                }
                if(this.cursor.up.isDown){
                    this.player.body.velocity.y += -200;
                }
                if(this.cursor.down.isDown){
                    this.player.body.velocity.y += 200;
                }
            } else {
                if(this.cursor.left.isDown){
                    this.player.body.velocity.x += -100;
                }
                if(this.cursor.right.isDown && this.player.x < fieldWidth - 16){
                    this.player.body.velocity.x += 100;
                }
                if(this.cursor.up.isDown){
                    this.player.body.velocity.y += -100;
                }
                if(this.cursor.down.isDown){
                    this.player.body.velocity.y += 100;
                }
            }
        }
        if(game.input.keyboard.isDown(Phaser.Keyboard.SHIFT)){
            this.hitbox.alpha = 1;
        } else {
            this.hitbox.alpha = 0;
        }
        this.hitbox.x = this.player.x;
        this.hitbox.y = this.player.y;
        if(this.playerPower >= 16){
            this.laser.x = this.player.x;
            this.laser.y = this.player.y + 20;
        }

        // Fire your bullets.
        if (game.input.keyboard.isDown(Phaser.Keyboard.Z)){
            this.fire();
            // this.explode(this.player.x, this.player.y);
        }

        // Ultimate skill.
        if (game.input.keyboard.isDown(Phaser.Keyboard.X)){
            this.playerSkill();
        }

        // Kill the player.
        if (game.input.keyboard.isDown(Phaser.Keyboard.C)){
            this.killPlayer();
        }

        // Destroy the enemies.
        if (game.input.keyboard.isDown(Phaser.Keyboard.G)){
            this.destroyAllEnemy();
        }

        // Game over.
        if (game.input.keyboard.isDown(Phaser.Keyboard.ENTER)){
            this.restart();
        }
        if(game.input.keyboard.isDown(Phaser.Keyboard.ESC)){
            if(this.gameover){
                this.bgm.stop();
                game.state.start('menu');
            } else {
                game.paused = true;
                this.stateLabel.text = 'Paused\nPress Enter to Resume';
            }
        }

        // Destroy killed objects.
        // this.destroyKilledObj();
        // console.log(this.enemies.length);

        // Revive the player.
        if(!this.player.alive && game.time.now > this.relifeTime){
            this.revivePlayer();
        }

        // Check if the player is just revived.
        if(game.time.now < this.unbeatableTime){
            if(game.time.now % 400 < 200) this.player.alpha = 0.8;
            else this.player.alpha = 0.4;
        } else {
            this.player.alpha = 1;
        }

        this.bulletAngle = (this.bulletAngle+3)%360;

        // Auto-aiming bullets.
        var tmp = this.enemies.length + this.bosses.length;
        var firstEnemy;
        if(tmp > 0){
            firstEnemy = this.enemies.getFirstAlive();
            if(!firstEnemy) firstEnemy = this.bosses.getFirstAlive();
            this.aimingBullets.forEachAlive(function(bullet){
                if(firstEnemy) game.physics.arcade.moveToObject(bullet, firstEnemy, 700);
                else {
                    bullet.body.velocity.x = 0;
                    bullet.body.velocity.y = -700;
                }
            });
        }

        // Change volume.
        if (game.input.keyboard.isDown(Phaser.Keyboard.U)) this.changeBGMVolume(0.002);
        if (game.input.keyboard.isDown(Phaser.Keyboard.I)) this.changeBGMVolume(-0.002);
        if (game.input.keyboard.isDown(Phaser.Keyboard.O)) this.changeVolume(0.002);
        if (game.input.keyboard.isDown(Phaser.Keyboard.P)) this.changeVolume(-0.002);

        // Run collisions.
        game.physics.arcade.overlap(this.playerBullets, this.enemies, this.playerHitsEnemy, null, this);
        game.physics.arcade.overlap(this.aimingBullets, this.enemies, this.playerHitsEnemy, null, this);
        game.physics.arcade.overlap(this.laser, this.enemies, this.playerHitsEnemy, null, this);
        game.physics.arcade.overlap(this.playerBullets, this.bosses, this.playerHitsEnemy, null, this);
        game.physics.arcade.overlap(this.aimingBullets, this.bosses, this.playerHitsEnemy, null, this);
        game.physics.arcade.overlap(this.laser, this.bosses, this.playerHitsEnemy, null, this);
        game.physics.arcade.overlap(this.player, this.items, this.eatItem, null, this);
        game.physics.arcade.overlap(this.hitbox, this.enemies, this.killPlayer, null, this);
        game.physics.arcade.overlap(this.hitbox, this.bosses, this.killPlayer, null, this);
        game.physics.arcade.overlap(this.hitbox, this.enemyBullets, this.killPlayer, null, this);
    },

    fire: function(){
        // Player is dead.
        if(!this.player.alive) return;
        // Not cooled down.
        if(this.nextShotAt > game.time.now) return;
        // No bullets left.
        if(this.playerBullets.countDead() == 0) return;

        this.nextShotAt = game.time.now + this.shotDelay;
        // Power up: 8，16，32，48，64，80，96，128
        if(this.playerPower < 32){
            // Level 1.
            var bullet = this.playerBullets.getFirstExists(false);
            bullet.reset(this.player.x, this.player.y - 20);
            bullet.body.velocity.y = (game.input.keyboard.isDown(Phaser.Keyboard.SHIFT)) ? -1000 : -700;
            bullet.angle = this.bulletAngle;
        } else if(this.playerPower < 48){
            // Level 2.
            for(var i=0;i<2;i++){
                var bullet = this.playerBullets.getFirstExists(false);
                bullet.reset(this.player.x + (i-0.5) * 10, this.player.y - 20);
                bullet.body.velocity.y = (game.input.keyboard.isDown(Phaser.Keyboard.SHIFT)) ? -1000 : -700;
                bullet.angle = this.bulletAngle;
                bullet.body.velocity.x = (i-0.5) * 30;
            }
        } else if(this.playerPower < 128){
            // Level 3.
            for(var i=0;i<3;i++){
                var bullet = this.playerBullets.getFirstExists(false);
                bullet.reset(this.player.x + (i-1) * 10, this.player.y - 20);
                bullet.body.velocity.y = (game.input.keyboard.isDown(Phaser.Keyboard.SHIFT)) ? -1000 : -700;
                bullet.angle = this.bulletAngle;
                bullet.body.velocity.x = (i-1) * 30;
            }
        } else {
            // Level 4.
            for(var i=0;i<5;i++){
                var bullet = this.playerBullets.getFirstExists(false);
                if(bullet){
                    bullet.reset(this.player.x + (i-2) * 10, this.player.y - 20);
                    bullet.body.velocity.y = (game.input.keyboard.isDown(Phaser.Keyboard.SHIFT)) ? -1000 : -700;
                    bullet.angle = this.bulletAngle;
                    bullet.body.velocity.x = (i-2) * 30;
                }
            }
        }

        this.plshotSE.play();

        this.aimingBulletsCount++;
        if(this.playerPower < 8) return;
        else if(this.playerPower < 64 && this.aimingBulletsCount < 5) return;
        else if(this.playerPower < 80 && this.aimingBulletsCount < 4) return;
        else if(this.aimingBulletsCount < 3) return;
        this.aimingBulletsCount = 0;

        for(var i=0;i<2;i++){
            var bullet = this.aimingBullets.getFirstExists(false);
            bullet.reset(this.player.x + (i-0.5) * 50, this.player.y - 20);
            bullet.body.velocity.y = (game.input.keyboard.isDown(Phaser.Keyboard.SHIFT)) ? -1000 : -700;
            bullet.angle = this.bulletAngle;
            bullet.body.velocity.x = (i-0.5) * 30;
        }
    },
    playerHitsEnemy: function(playerBullet, enemy){
        // Kill the bullet and the enemy.
        if(enemy.alive) {
            if(playerBullet.key === 'laser') enemy.damage(3);
            else {
                enemy.damage(50);
                playerBullet.kill();
            }
        }

        // Play explosion animation.
        if(!enemy.alive){
            // this.explode(enemy.x, enemy.y);
            enemy.kill();

            this.emitter = this.emitters.getFirstExists(false);
            this.emitter = game.add.emitter(422, 320, 15);
            this.emitter.makeParticles('pixel');
            this.emitter.setYSpeed(-150, 150);
            this.emitter.setXSpeed(-150, 150);
            this.emitter.setScale(2, 0, 2, 0, 800);
            this.emitter.gravity = 500;
            this.emitter.x = enemy.x;
            this.emitter.y = enemy.y;
            this.emitter.start(true, 800, null, 15);
            this.emitter.mask = this.mask;

            // Drop item.
            if(enemy.key === "boss"){
                for(var i=0;i<4;i++){
                    this.rand_x = game.rnd.integerInRange(-50, 50);
                    this.rand_y = game.rnd.integerInRange(-50, 50);
                    this.dropItem(enemy.x+this.rand_x, enemy.y+this.rand_y, 'power', 1, 0, -120);
                }
                this.rand_x = game.rnd.integerInRange(-50, 50);
                this.rand_y = game.rnd.integerInRange(-50, 50);
                this.dropItem(enemy.x+this.rand_x, enemy.y+this.rand_y, 'power', 3, 0, -120);
            } else {
                this.randDropItem = game.rnd.integerInRange(1, 30);
                if(this.randDropItem <= 19) {}
                else if(this.randDropItem <= 24) this.dropItem(enemy.x, enemy.y, 'power', 1, 0, -120);
                else if(this.randDropItem <= 29) this.dropItem(enemy.x, enemy.y, 'point', 2, 0, -120);
                else if(this.randDropItem <= 30) this.dropItem(enemy.x, enemy.y, 'power', 3, 0, -120);
            }

            if(enemy.key === "boss") this.addScore(5000);
            else if(enemy.key === "enemy-m") this.addScore(1000);
            else this.addScore(500);

            if(enemy.key === "boss") this.bossdeadSE.play();
            else this.explosionSE.play();

            // Update health bar.
            if(enemy.key === "boss") this.bossHealthBar.scale.x = 0;
        }
        else {
            if(playerBullet.key !== 'laser') this.spark(enemy.x, enemy.y);
            this.addScore(10);

            if(playerBullet.key !== 'laser') this.damageSE.play();

            // Update health bar.
            if(enemy.key === 'boss') this.bossHealthBar.scale.x = enemy.health/enemy.maxHealth;
        }
    },
    explode: function(x, y){
        var explosion = game.add.sprite(x, y, 'explode');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);
    },
    spark: function(x, y){
        var spark = game.add.sprite(x, y, 'sparkle');
        spark.anchor.setTo(0.5);
        spark.animations.add('spark');
        spark.play('spark', 24, false, true);
    },
    killPlayer: function(){
        // Player is dead.
        if(!this.player.alive) return;
        // Player is unbeatable.
        if(this.unbeatableTime > game.time.now) return;

        this.splitItems(this.playerPower);

        this.player.kill();
        this.hitbox.kill();
        this.laser.kill();
        this.relifeTime = game.time.now + 1000;
        this.unbeatableTime = game.time.now + 5000;
        this.explode(this.player.x, this.player.y);

        this.life = this.lives.getFirstAlive();
        if(this.life){
            this.life.kill();
        }
        if(this.lives.countLiving() == 0){
            // console.log("You're dead.");
            this.stateLabel.text = 'Game Over\nPress Enter to Restart\nPress Esc to Menu';
            this.gameover = true;

            // Write score to database.
            setTimeout( () => {
                var name = "";
                name = prompt("Please enter your name (Your score: " + this.score + ")", "");
                this.databaseWrite(this.score, name);
            }, 1000);
        } else {
            this.bombs.callAll('revive');
        }

        this.playerPower -= 16;
        if(this.playerPower < 0) this.playerPower = 0;
        this.powerLabel.text = this.playerPower;

        this.pldeadSE.play();
    },
    revivePlayer: function(){
        if(this.lives.countLiving() == 0) return;

        this.player.revive();
        this.player.x = fieldWidth/2;
        this.player.y = gameHeight/2 + 200;
        this.hitbox.revive();
        this.hitbox.x = fieldWidth/2;
        this.hitbox.y = gameHeight/2 + 200;
        if(this.playerPower >= 16) {
            this.laser.revive();
            this.laser.x = this.player.x;
            this.laser.y = this.player.y + 20;
        }
    },
    updateTimer: function(){
        this.timer++;
        if(this.timer <= 20){
            if(this.timer%2 == 0) this.createEnemy1(200, -50, 0, 50, -10, 0, 50*this.level, 'enemy-s');
            if(this.timer%2 == 1) this.createEnemy1(240, -50, 0, 50, 10, 0, 50*this.level, 'enemy-s');
        }
        else if(this.timer <= 40){
            this.rand_x = game.rnd.integerInRange(0, 440);
            this.rand_ax = game.rnd.integerInRange(-10, 10)
            this.createEnemy1(this.rand_x, -50, 0, 50, this.rand_ax, 0, 100*this.level, 'enemy-l');
        }
        else if(this.timer <= 52){
            if(this.timer%2 == 0) this.createEnemy1(-32, 250, 100, 0, 0, 0, 200*this.level, 'enemy-m');
            if(this.timer%2 == 1) this.createEnemy1(440+32, 150, -100, 0, 0, 0, 200*this.level, 'enemy-m');
            
            this.rand_x = game.rnd.integerInRange(50, 390);
            if(this.timer%3 == 0) this.createEnemy1(this.rand_x, -50, 0, 50, 0, 0, 100*this.level, 'enemy-xs');
        }
        else if(this.timer == 61){
            // Generate boss.
            this.createBoss(220, -50, 0, 300, 0, -300, 4000*this.level, 'boss');
        }
        else {
            // this.timer = 0;
            // this.level++;
            if(this.timer == 63){
                this.boss = this.bosses.getFirstAlive();
                if(this.boss){
                    this.boss.body.velocity.y = 0;
                    this.boss.body.acceleration.y = 0;
                }
            }
            if(this.timer % 4 == 0){
                this.randBossFire();
            }
            if(this.timer % 4 == 2){
                this.bossMove();
            }
        }
        
        // Enemy control.
        this.enemies.forEachAlive((enemy)=>{
            if(enemy.firetime == this.timer){
                if(enemy.key === 'enemy-s') this.enemyFire(enemy.x, enemy.y);
                else if(enemy.key === 'enemy-xs'){
                    for(var i=0;i<7;i++){
                        this.enemyFire2(enemy.x, enemy.y, 60+i*10);
                    }
                }
                enemy.firetime = -1;
            }
            if(enemy.movetime1 == this.timer){
                if(enemy.key === 'enemy-xs') enemy.body.velocity.y = 0;
                enemy.movetime1 = -1;
            }
            if(enemy.movetime2 == this.timer){
                this.rand = game.rnd.integerInRange(-1, 0);
                if(enemy.key === 'enemy-xs') enemy.body.velocity.x = (this.rand*2+1)*50;
                enemy.movetime2 = -1;
            }
        });
    },
    createEnemy1: function(x, y, vx, vy, ax, ay, health, name){
        let enemy = this.enemies.create(x, y, name);
        enemy.anchor.setTo(0.5, 0.5);
        enemy.animations.add('fly', [ 0, 1, 2, 3, 4 ], 10, true);
        enemy.play('fly');
        enemy.body.velocity.x = vx;
        enemy.body.velocity.y = vy;
        enemy.body.acceleration.x = ax;
        enemy.body.acceleration.y = ay;
        enemy.health = health;
        enemy.lifespan = 12000;
        enemy.outOfBoundsKill = true;
        enemy.checkWorldBounds = true;
        enemy.events.onKilled.add( () => {
            // clearInterval(enemy.shot);
            setTimeout( () => {
                enemy.destroy();
            }, 1000);
        }, this);
        
        enemy.firetime = this.timer + 6;
        enemy.movetime1 = this.timer + 6;
        enemy.movetime2 = this.timer + 8;
    },
    createBoss: function(x, y, vx, vy, ax, ay, health, name){
        let enemy = this.bosses.create(x, y, name);
        enemy.anchor.setTo(0.5, 0.5);
        enemy.animations.add('fly', [ 0, 1, 2, 3, 4 ], 10, true);
        enemy.play('fly');
        enemy.body.velocity.x = vx;
        enemy.body.velocity.y = vy;
        enemy.body.acceleration.x = ax;
        enemy.body.acceleration.y = ay;
        enemy.health = health;
        enemy.maxHealth = health;
        enemy.collideWorldBounds = true;
        enemy.events.onKilled.add( () => {
            setTimeout( () => {
                enemy.destroy();
            }, 1000);
            // Reset stage.
            this.timer = 0;
            this.level++;
            if(this.level > 10) this.level = 10;
        }, this);
        this.bossHealthBar.scale.x = 1;
    },
    dropItem: function(x, y, itemName, itemID, vx, vy){
        let item = this.items.create(x, y, itemName);
        item.anchor.setTo(0.5);
        item.body.velocity.x = vx;
        item.body.velocity.y = vy;
        item.body.acceleration.y = 150;
        item.lifespan = 12000;
        item.health = itemID;
        if(itemID == 3) item.scale.setTo(1.2);
        else item.scale.setTo(0.9);
        item.events.onKilled.add( () => {
            setTimeout( () => {
                item.destroy();
            }, 1000);
        }, this);
    },
    eatItem: function(player, item){
        // Player is dead.
        if(!this.player.alive) return;
        
        this.oldPlayerPower = this.playerPower;
        this.value = 0;

        // Power item.
        if(item.health == 1){
            this.playerPower += 1;
            if(this.playerPower >= 128) this.playerPower = 128;
            this.powerLabel.text = this.playerPower;
            if(this.playerPower >= 128) this.powerLabel.text = 'MAX';
            this.value = 10;
        }
        // Point item.
        else if(item.health == 2){
            this.playerPoint += 1;
            this.pointLabel.text = this.playerPoint;
            this.value = (500 - Math.round(item.y))*10;
        }
        // Big power item.
        else if(item.health == 3){
            this.playerPower += 8;
            if(this.playerPower >= 128) this.playerPower = 128;
            this.powerLabel.text = this.playerPower;
            if(this.playerPower >= 128) this.powerLabel.text = 'MAX';
            this.value = 10;
        }

        if(item.health == 1 || item.health == 3){
            // Power up sound effect.
            if(this.oldPlayerPower < 8 && this.playerPower >= 8) this.powerupSE.play();
            else if(this.oldPlayerPower < 16 && this.playerPower >= 16) this.powerupSE.play();
            else if(this.oldPlayerPower < 32 && this.playerPower >= 32) this.powerupSE.play();
            else if(this.oldPlayerPower < 48 && this.playerPower >= 48) this.powerupSE.play();
            else if(this.oldPlayerPower < 64 && this.playerPower >= 64) this.powerupSE.play();
            else if(this.oldPlayerPower < 80 && this.playerPower >= 80) this.powerupSE.play();
            else if(this.oldPlayerPower < 128 && this.playerPower >= 128) this.powerupSE.play();
        }

        this.addScore(this.value);
        this.addTextBox(item.x, item.y, this.value);
        item.kill();
        this.itemSE.play();

        // Laser beam.
        if(this.playerPower >= 16 && !this.laser.alive){
            this.laser.revive();
            this.laser.x = this.player.x;
            this.laser.y = this.player.y + 20;
        }
    },
    playerSkill: function(){
        // Player is dead.
        if(!this.player.alive) return;
        // Not cooled down.
        if(this.skillTime > game.time.now) return;

        this.skillTime = game.time.now + 2000;
        this.bomb = this.bombs.getFirstAlive();
        if(this.bombs.countLiving() == 0){
            return;
        }
        if(this.bomb) this.bomb.kill();

        for(var i=0;i<20;i++){
            var bullet = this.playerBullets.getFirstExists(false);
            bullet.reset(this.player.x, this.player.y - 20);
            bullet.body.velocity.x = game.rnd.integerInRange(-200, 200);
            bullet.body.velocity.y = game.rnd.integerInRange(-1000, -700);
        }

        this.unbeatableTime = game.time.now + 3000;

        this.skillSE.play();
    },
    splitItems: function(playerPower){
        if(playerPower == 0) return;

        // Drop item.
        this.num = playerPower;
        if(this.num > 8){
            this.rand_vx = game.rnd.integerInRange(-50, 50);
            this.rand_vy = game.rnd.integerInRange(-200, -120);
            this.dropItem(this.player.x, this.player.y, 'power', 3, this.rand_vx, this.rand_vy);
            this.num -= 8;
        }
        this.num = Math.min(this.num, 5);
        for(var i=0;i<this.num;i++){
            this.rand_vx = game.rnd.integerInRange(-50, 50);
            this.rand_vy = game.rnd.integerInRange(-200, -120);
            this.dropItem(this.player.x, this.player.y, 'power', 1, this.rand_vx, this.rand_vy);
        }
    },
    addScore: function(value){
        this.score += value;
        this.scoreLabel.text = this.score;
        if(this.score > this.highscore){
            this.highscore = this.score;
            this.highscoreLabel.text = this.highscore;
        }
    },
    changeVolume: function(value){
        if(this.explosionSE.volume + value >= 1) value = 1 - this.explosionSE.volume;
        if(this.explosionSE.volume + value <= 0) value = 0 - this.explosionSE.volume;
        this.explosionSE.volume += value;
        this.itemSE.volume += value;
        this.pldeadSE.volume += value;
        this.plshotSE.volume += value;
        this.powerupSE.volume += value;
        this.skillSE.volume += value;
        this.damageSE.volume += value;
        this.enemyshotSE.volume += value;
        this.bossdeadSE.volume += value;
        this.soundVolume = Math.round(this.explosionSE.volume * 100);
        this.volumeLabel.text = this.soundVolume;
    },
    changeBGMVolume: function(value){
        if(this.bgm.volume + value >= 1) value = 1 - this.bgm.volume;
        if(this.bgm.volume + value <= 0) value = 0 - this.bgm.volume;
        this.bgm.volume += value;

        this.bgmVolume = Math.round(this.bgm.volume * 100);
        this.bgmVolumeLabel.text = this.bgmVolume;
    },
    addTextBox: function(x, y, value){
        // this.textBox = game.add.text(x, y, value, {font: '12px Arial', fill: '#ffffff'});
        // this.textBox.lifespan = 1000;
    },
    destroyAllEnemy: function(){
        this.enemies.forEach(function(enemy){
            var explosion = game.add.sprite(enemy.x, enemy.y, 'explode');
            explosion.anchor.setTo(0.5);
            explosion.animations.add('boom');
            explosion.play('boom', 15, false, true);
            enemy.destroy();
        });
    },
    restart: function(){
        if(!this.gameover) return;

        this.enemies.forEach(
            (enemy) => {
                enemy.kill();
            }
        );
        this.bosses.forEach(
            (boss) => {
                boss.kill();
            }
        );
        this.bossHealthBar.scale.x = 0;
        this.enemyBullets.forEach(
            (bullet) => {
                bullet.kill();
            }
        );
        this.items.forEach(
            (item) => {
                item.kill();
            }
        );
        this.player.revive();
        this.player.x = fieldWidth/2;
        this.player.y = gameHeight/2 + 200;
        this.hitbox.revive();
        this.hitbox.x = fieldWidth/2;
        this.hitbox.y = gameHeight/2 + 200;
        this.lives.callAll('revive');
        this.bombs.callAll('revive');
        this.timer = 0;
        this.stateLabel.text = '';
        this.score = 0;
        this.scoreLabel.text = this.score;
        this.playerPower = 0;
        this.powerLabel.text = this.playerPower;
        this.playerPoint = 0;
        this.pointLabel.text = this.playerPoint;

        this.gameover = false;
        this.level = 1;
    },
    enemyFire: function(x, y){
        this.enemyBullet = this.enemyBullets.getFirstExists(false);

        if (!this.enemyBullet) return;
        
        this.enemyBullet.reset(x, y);
        game.physics.arcade.moveToObject(this.enemyBullet, this.player, 120);

        this.enemyshotSE.play();
    },
    enemyFire2: function(x, y, angle){
        this.enemyBullet = this.enemyBullets.getFirstExists(false);

        if (!this.enemyBullet) return;
        
        this.enemyBullet.reset(x, y);
        this.enemyBullet.body.velocity.x = 100*Math.cos(angle * Math.PI/180);
        this.enemyBullet.body.velocity.y = 100*Math.sin(angle * Math.PI/180);

        this.enemyshotSE.play();
    },
    resume: function(){
        if(!game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) return;
        game.paused = false;
        this.stateLabel.text = '';
    },
    bossMove: function(){
        this.boss = this.bosses.getFirstAlive();
        if(!this.boss) return;
        this.rand_x = game.rnd.integerInRange(50, 390);
        this.rand_y = game.rnd.integerInRange(50, 200);
        game.add.tween(this.boss).easing(Phaser.Easing.Exponential.In).to({x: this.rand_x, y: this.rand_y}).start();
    },
    randBossFire: function(){
        this.rand = game.rnd.integerInRange(0, 1);
        if(this.rand == 0) this.bossFire1();
        else if(this.rand == 1) this.bossFire2();
    },
    bossFire1: function(){
        this.boss = this.bosses.getFirstAlive();
        if(!this.boss) return;
        for(var i=0;i<40;i++){
            this.enemyBullet = this.enemyBullets.getFirstExists(false);
            if (this.enemyBullet){
                this.enemyBullet.reset(this.boss.x, this.boss.y);
                this.enemyBullet.body.velocity.x = 100*Math.cos(9*i * Math.PI/180);
                this.enemyBullet.body.velocity.y = 100*Math.sin(9*i * Math.PI/180);
            }
        }

        this.enemyshotSE.play();
    },
    bossFire2: function(){
        this.boss = this.bosses.getFirstAlive();
        if(!this.boss) return;
        for(var i=0;i<40;i++){
            this.enemyBullet = this.enemyBullets.getFirstExists(false);
            if (this.enemyBullet){
                this.enemyBullet.reset(this.boss.x, this.boss.y);
                this.rand_vx = game.rnd.integerInRange(-80, 80);
                this.rand_vy = game.rnd.integerInRange(-300, -50);
                this.rand_ay = game.rnd.integerInRange(150, 300);
                this.enemyBullet.body.velocity.x = this.rand_vx;
                this.enemyBullet.body.velocity.y = this.rand_vy;
                this.enemyBullet.body.acceleration.y = this.rand_ay;
            }
        }

        this.enemyshotSE.play();
    },
    databaseWrite: function(score, name){
        var leaderboardRef = firebase.database().ref('leaderboard');
        leaderboardRef.push({
            score: score,
            name: name
        });
    },
    showHighscore: function(){}
};